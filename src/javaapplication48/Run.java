package javaapplication48;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author alex
 */
public class Run {
    
    public static void main(String[] args) {
        List<Creature> list = new ArrayList<>();
        
        Creature myCreature = new Creature("Joda");
        
        
        
        Cat myCat = new Cat("Kitty");
        
        Persian myPersianCat = new Persian("BlueKitty");
        
        Human myHuman = new Human("Alex", myPersianCat);
        
        
        list.add(myCreature);
        list.add(myHuman);
        list.add(myCat);
        list.add(myPersianCat);
        
        for (Creature c : list) {
            System.out.println(c.toString());
        }
        
        
        
    }
}
