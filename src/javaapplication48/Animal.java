package javaapplication48;

/**
 *
 * @author alex
 */
public class Animal extends Creature {

    public Animal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Animal{name=" + super.getName() + "}"; //To change body of generated methods, choose Tools | Templates.
    }

}
