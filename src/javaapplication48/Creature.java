package javaapplication48;

/**
 *
 * @author alex
 */
public class Creature {
    private String name;

    public Creature(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    @Override    
    public String toString() {
        return "Creature{name = "+ this.name  +'}';
    }
    
    
    
}
