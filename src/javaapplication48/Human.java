package javaapplication48;

/**
 *
 * @author alex
 */
public class Human extends Creature {

    Cat pet;

    public Human(String name, Cat pet) {
        super(name);
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Human{name = " + super.getName() + ", has a pet --> "+this.pet.toString()+"}"; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
