package javaapplication48;

/**
 *
 * @author alex
 */
public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Cat{name = " + super.getName() + "}"; //To change body of generated methods, choose Tools | Templates.
    }

}
